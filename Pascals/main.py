import timeit


# What's the biggest row you want to go to?
ROW_NUMBER = 128


def rowN(n):
    """
    This is my first crack at calculating the number of odds in the nth row of pascals triangle. It's not great speedwise.

    Args:
        n ([int]): [which row are you interested in.]

    Returns:
        ([tuple(int, list, int)]): [the number of odds in the row, the row, the total number of numbers in the row]
    """

    count_odd = 1
    row = [1]

    # i = 1,2,3,4
    for i in range(1, n + 1):
        # [0,1,2,3,4]
        # [1, , , , ]
        row.insert(i, row[i - 1] * (n - i + 1) // i)

        if row[i] % 2 != 0:
            count_odd += 1

    return count_odd, row, n + 1


def oddsN(n):
    """ 
    This is a fast method for calculating the number of odds in the nth row of pascals triangle. 
    
    a(n) = 2^(number of 1's in binary form of (n-1))

    Shamelessles borrwed from OEIS and Gabriel C. Benamy

    Args:
        n int: Which row are you interested in?

    Returns:
        int: The number of odds in row n
    """
    return 2 ** bin(n - 1)[2:].count("1")


# start_time = timeit.default_timer()
# pascals_odd = sum(list(zip(*[rowN(n) for n in range(129)]))[0])
# print(f"The time difference is : {timeit.default_timer() - start_time}")


# time everything, this is the start time
start_time = timeit.default_timer()


# Total number of numbers from row 0 to row n
total_n = ((ROW_NUMBER) * (ROW_NUMBER + 1)) // 2

# What's the sum of the odds from 0,127
quick_odd = sum(oddsN(n) for n in range(ROW_NUMBER))

# Print is as a percentage
print(f"{quick_odd/total_n:.3%}")

# Print the time it took
print(f"The time difference is : {timeit.default_timer() - start_time}")
