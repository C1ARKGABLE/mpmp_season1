# Welcome to Gable's Puzzle Solutions

## What puzzles?
These are my attempted solutions for some of [Matt Parker's Maths Puzzles](https://web.archive.org/web/20201127064046/http://www.think-maths.co.uk/maths-puzzles) Season 1!

## Why these puzzles?
I follow [Matt Parker](https://twitter.com/standupmaths) and [his interesting forays into recreational mathematics](https://www.youtube.com/user/standupmaths/). These are just some of the puzzles that piqued my interest.

## Python?
Yup, this is Python. 🐍