from collections import defaultdict
from pprint import pprint
from graphviz import Graph


def dfs(graph, rang):
    solutions = []

    for i in rang:
        visited = []
        queue = [i]
        while queue:
            node = queue.pop()
            if node not in visited:
                visited.append(node)
                for neighbor in graph[node]:
                    queue.append(neighbor)
        if len(visited) == len(rang) and check(visited, primes):
            return visited


def check(sol, primes):
    for idx, x in enumerate(sol):
        try:
            if x + sol[idx + 1] not in primes:
                return False
        except IndexError:
            return True


def prime_components(components):

    prime_components = defaultdict(list)
    graph = defaultdict(list)

    components = list(components)

    components.sort()
    max_val = components[-1] + components[-2] + 1
    primes = set(findPrimes(max_val))

    for i in components:
        for j in components:
            if j >= i:
                break
            if (x := i + j) in primes:
                prime_components[x].append((str(i), str(j)))
                graph[i].append(j)
                graph[j].append(i)
    edges = []
    [edges.extend(i) for i in prime_components.values()]
    return edges, graph, primes


def findPrimes(n):
    """ Returns  a list of primes < n """
    sieve = [True] * n
    for i in range(3, int(n ** 0.5) + 1, 2):
        if sieve[i]:
            sieve[i * i :: 2 * i] = [False] * ((n - i * i - 1) // (2 * i) + 1)
    return [2] + [i for i in range(3, n, 2) if sieve[i]]


rang = range(1, 10)


edges, graph, primes = prime_components(rang)

gr = Graph(name="bob")
gr.edges(edges)
gr.view()

sol = dfs(graph, rang)
pprint(sol)
