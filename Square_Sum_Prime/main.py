def prime(i, primes):
    for prime in primes:
        if not (i == prime or i % prime):
            return False
    primes.add(i)
    return i


def historic(n):
    primes = set([2])
    i, p = 2, 0
    while True:
        if prime(i, primes):
            p += 1
            if p == n:
                return primes
        i += 1


def checkSumSquare(n, primes):
    # Really snhouldn't recalculate the sum everytime from scratch...
    # We're summing 2+3+5+7... everytime, when we should start adding from the last prime.
    return sum(prime ** 2 for prime in primes) % n == 0


x = 20598

primes_all = list(historic(x))
primes_all.sort()

for i in range(2, x):
    if checkSumSquare(i, primes_all[:i]):
        print(i)

